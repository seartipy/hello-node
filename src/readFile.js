var fs = require('fs');

var readFile = function(fd, cb) {
    var buffers = [];
    var read_impl = function() {
        var buffer = new Buffer(4096);
        return fs.read(fd, buffer, 0, buffer.length, null, read_cb);
    };
    var read_cb = function(err, bytesRead, buffer) {
        if (err) {
            fs.close(fd);
            return cb(err);
        }
        if (bytesRead === 0) {
            fs.close(fd);
            return cb(null, Buffer.concat(buffers));
        }
        buffers.push(buffer.slice(0, bytesRead));
        return read_impl();
    };
    read_impl();
};

fs.open(process.argv[2], 'r', function(err, fd){
    if (err) {
        console.error("error opening file " + err);
        return;
    }
    readFile(fd, function(err, buffer) {
        if (err) {
            console.error("error reading file " + err);
            return;
        }
        console.log(buffer.toString());
    });
});
