#include <iostream>

int factorial(int n)
{
  auto fact = 1;
  for(auto i = 2; i <= n; ++i)
    fact *= i;
  return fact;
}

#include <string>
#include <cassert>
#include <sstream>

int main(int argc, char *argv[])
{
  assert(argc > 1);
  std::string arg(argv[1]);
  std::istringstream in(arg);
  int n;
  in >> n;
  std::cout << factorial(n);
}
