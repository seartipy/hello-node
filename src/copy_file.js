var fs = require('fs');

var buffers = [];
var reading_done = false;
var write_done = function() {
    return buffers.length === 0 && reading_done;
};

var open_files_for_copy = function(in_file, out_file, callback) {
    return fs.open(in_file, 'r', function(err, in_fd) {
        if (err) {
            callback(err);
            return;
        }
        fs.open(out_file, 'w', function(err, out_fd) {
            if (err) {
                callback(err);
                return;
            }
            callback(null, in_fd, out_fd);
        });
    });
};

var write_all_buffers = function(outfd) {
    var write_callback = function(err, written, buffer) {
        if (err) {
            console.error('error writing file' + err);
            process.exit(1);
        }
        buffers[0].written += written;
        if (buffers[0].written >= buffers[0].buffer.length)
            buffers.shift();
        if (buffers.length !== 0)
            write_all_buffers();
        if (write_done()) {
            fs.close(outfd);
            process.exit(0);
        }
    };
    var t = buffers[0];
    return fs.write(outfd,
                    t.buffer,
                    t.written,
                    t.buffer.length - t.written,
                    null,
                    write_callback);
};

var read_into_buffers = function(infd, outfd) {
    var read_callback = function(err, bytesRead, buffer) {
        if (err) {
            console.error('error reading file ' + err);
            process.exit(1);
        }
        if (bytesRead === 0) {
            reading_done = true;
            return;
        }
        buffers.push({
            written: 0,
            buffer: buffer.slice(0, bytesRead)
        });
        write_all_buffers(outfd);
        read_into_buffers(infd, outfd);
    };
    var buffer = new Buffer(4096);
    return fs.read(infd, buffer, 0, buffer.length, null, read_callback);
};

var copy_files = function(in_file, out_file) {
    return open_files_for_copy(in_file, out_file, function(err, infd, outfd) {
        if (err) {
            console.error('error opening files : ' + err);
            return;
        }
        read_into_buffers(infd, outfd);
    });
};

copy_files(process.argv[2], process.argv[3]);
