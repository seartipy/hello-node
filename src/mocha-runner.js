var fs = require('fs');
var cp = require('child_process');

fs.watch('.', function(event, filename) {
    console.log(filename);
    if(filename.indexOf('_test.js') === filename.length - 8)
        cp.exec('mocha ' + filename, function(err, output) {
            if(err)
                console.error(err);
            else
                console.log(output);
        });
});
